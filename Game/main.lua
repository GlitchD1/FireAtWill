
version = { release = 0, revision = 1 }
scene_ = nil

Music = require "Scripts/Music"

function Start()
	graphics.windowTitle = "Fire-at-Will V"..version.release.."."..version.revision
	
	input.mouseVisible = true
	
	CreateConsoleAndDebugHud()
	CreateText()
	CreateScene()
	
	SetupViewport()
	
	Music.Init(scene_)
	
	Music.Play("menu")
	
	SubscribeToEvents()
end

function CreateConsoleAndDebugHud()
	local uiStyle = cache:GetResource("XMLFile", "UI/DefaultStyle.xml")
	if uiStyle == nil then
		return
	end
	
	engine:CreateConsole()
	console.defaultStyle = uiStyle
	console.background.opacity = 1
	engine:CreateDebugHud()
	debugHud.defaultStyle = uiStyle
end

function CreateScene()
	scene_ = Scene()
	local file = cache:GetFile("Scenes/ShellMap.xml")
	scene_:LoadXML(file)
	file:delete()
end

function CreateText()
	local helloText = Text:new()
	helloText.text = "Hello World from Fire-at-Will!"
	helloText:SetFont(cache:GetResource("Font", "Fonts/Anonymous Pro.ttf"), 30)
	helloText.color = Color(0.0, 1.0, 0.0)
	helloText.horizontalAlignment = HA_CENTER
	helloText.verticalAlignment = VA_CENTER
	ui.root:AddChild(helloText)
end

function SubscribeToEvents()
	SubscribeToEvent("Update", "HandleUpdate")
	SubscribeToEvent("KeyDown", "HandleKeyDown")
	SubscribeToEvent("KeyUp", "HandleKeyUp")
end

function HandleUpdate()
	
end

function HandleKeyUp(eventType, eventData)
	local key = eventData["Key"]:GetInt()
	if key == KEY_ESCAPE then
		if console:IsVisible() then
		console.visible = false
		else
			engine:Exit();
		end
	end
end

function HandleKeyDown(eventType, eventData)
	local key = eventData["Key"]:GetInt()
	
	if key == KEY_F1 then
		console:Toggle()
	elseif key == KEY_F2 then
		debugHud:ToggleAll()
	end
end

function SetupViewport()
	local cameraNode = scene_:GetChild("Camera")
	local viewport = Viewport:new(scene_, cameraNode:GetComponent("Camera"))
	renderer:SetViewport(0, viewport)
end

