Fire-At-Will
-----
A modern-ish remake of the classic game Return Fire,
aka. GlitchD1's #devvinghour pet project


Linked List:

- Development chat happen here -> https://discord.gg/wT3y8kC
- Currently developed on the Urho3D engine - https://urho3d.github.io/
- 3D models developed using AssetForge Deluxe - https://assetforge.io/
- Some assets used from OpenGameArt.Org - https://opengameart.org/

